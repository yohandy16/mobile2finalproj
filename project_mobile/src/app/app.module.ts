import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { ProfilePage } from '../pages/profile/profile';
import { ContactPage } from '../pages/contact/contact';
import { HistoryPage } from '../pages/history/history';
import { AddPage } from '../pages/add/add';
import { HistoryDetailPage } from '../pages/history-detail/history-detail';
import { ContactDetailPage } from '../pages/contact-detail/contact-detail';
import { OngoingDetailPage } from '../pages/ongoing-detail/ongoing-detail';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { FIREBASE_CONFIG } from '../app/app.firebase.config';
import { DatabaseService } from '../services/database.service';
import { Http, HttpModule } from '@angular/http';
import { EditProfilePage } from '../pages/edit-profile/edit-profile';
import { Camera } from '@ionic-native/camera';
import { PopoverControllerPage } from '../pages/popover-controller/popover-controller';
import { TransactionService } from '../services/transaction.service';

import { ContactService } from '../services/contact.service';

import { AddContactPage } from '../pages/add-contact/add-contact';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    ProfilePage,
    AddPage,
    AddContactPage,
    ContactPage,
    HistoryPage,
    HistoryDetailPage,
    ContactDetailPage,
    OngoingDetailPage,
    LoginPage,
    RegisterPage,
    EditProfilePage,
    PopoverControllerPage,
    AddContactPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    AngularFireModule.initializeApp(FIREBASE_CONFIG.fire),
    AngularFireDatabaseModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    ProfilePage,
    ContactPage,
    HistoryPage,
    HistoryDetailPage,
    ContactDetailPage,
    OngoingDetailPage,
    LoginPage,
    RegisterPage,
    AddPage,
    AddContactPage,
    EditProfilePage,
    PopoverControllerPage,
    AddContactPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DatabaseService,
    TransactionService,
    ContactService,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AngularFireAuth,
    Camera
  ]
})
export class AppModule {}
