import { User } from "../data/user";
import { Http } from "@angular/http";
import { Injectable } from '@angular/core';
import firebase from 'firebase';
import {normalizeURL,ToastController} from 'ionic-angular'

@Injectable()
export class DatabaseService {

    private user: User[] = [];
    private users: any;
    private debt: number = 1000;
    private credit: number = 2000;

    constructor(private http: Http,public toastCtrl:ToastController) {
        this.users = firebase.database().ref(`users`);
        
    }

    //USER
    addUser(user: User) {
        // console.log(user);
        firebase.database().ref('users/' + user.uid).set(user);
    }

    getUsers() {
        return this.users;
    }

    getUserById(uid: string){
        let userRef: firebase.database.Reference = firebase.database().ref(`/users/`);
        let result = [];
        userRef.on('value', data => {
            data.forEach(element => {
                if (element.child(`/uid`).val() == uid) {
                    result.push(element.val());
                    return false;
                }
            });
        });
        return result;
    }

    getUserIdByUsername(username: string) {
        let userRef: firebase.database.Reference = firebase.database().ref(`/users/`);
        let result = '';
        userRef.on('value', data => {
            data.forEach(element => {
                if (element.child(`/uid`).val() == '5APzueU3wlMlfVaPnqgbD0UU2B32') { //ganti ke username nanti kalo udh ada username
                    result = element.child(`/email`).val();
                }
            });
        });
        return result;
    }

    getDebtById(uid: string){
        let userRef: firebase.database.Reference = firebase.database().ref(`/users/`);
        let result = [];
        userRef.on('value', data => {
            data.forEach(element => {
                if (element.child(`/uid`).val() == uid) {
                    result.push(element.child(`/debt_total`).val());
                    return false;
                }
            });
        });
        return result;
    }

    getCreditById(uid: string){
        let userRef: firebase.database.Reference = firebase.database().ref(`/users/`);
        let result = [];
        userRef.on('value', data => {
            data.forEach(element => {
                if (element.child(`/uid`).val() == uid) {
                    result = element.child(`/credit_total`).val();
                    return false;
                }
            });
        });
        return result;
    }

    // deleteUser(): void {
    //     const userRef: firebase.database.Reference = firebase.database().ref(`/users/` + user.uid);
    //     userRef.remove()
    //   }

    generateUniqueId(len: number) {
        var text = "";
        var charset = "abcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < len; i++)
            text += charset.charAt(Math.floor(Math.random() * charset.length));

        return text;
    }

    logout() {
        firebase.auth().signOut();
    }

    //CAMERA
    uploadImage(imageURI,transactionId){
        return new Promise<any>((resolve, reject) => {
          let storageRef = firebase.storage().ref();
          let imageRef = storageRef.child('image').child(transactionId);
          this.encodeImageUri(imageURI, function(image64){
            imageRef.putString(image64, 'data_url')
            .then(snapshot => {
              resolve(snapshot.downloadURL)
            }, err => {
              reject(err);
            })
          })
        })
      }

      uploadImageToFirebase(image,transactionId){
        image = normalizeURL(image);
      
        //uploads img to firebase storage
        this.uploadImage(image,transactionId)
        .then(photoURL => {
      
          let toast = this.toastCtrl.create({
            message: 'Image was updated successfully',
            duration: 3000
          });
          toast.present();
          })
        }

      encodeImageUri(imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext("2d");
        var img = new Image();
        img.onload = function () {
          var aux:any = this;
          c.width = aux.width;
          c.height = aux.height;
          ctx.drawImage(img, 0, 0);
          var dataURL = c.toDataURL("image/jpeg");
          callback(dataURL);
        };
        img.src = imageUri;
      };


}