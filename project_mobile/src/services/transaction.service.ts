import { Http } from "@angular/http";
import { Injectable } from '@angular/core';
import firebase from 'firebase';
import { Transaction } from "../data/transaction";

@Injectable()
export class TransactionService {

    private transactions: any = [];
    private currentUser = firebase.auth().currentUser;

    constructor(private http: Http) {
        this.transactions = firebase.database().ref(`transaction_history`);
    }

    addTransaction(transaction: any) {
        let uid = this.generateUniqueId(10);
        transaction.uid = uid;
        firebase.database().ref('transaction_history/' + uid).set(transaction);
    }

    editTransaction(transaction: any) {
        let uid = transaction.uid;
        firebase.database().ref('transaction_history/' + uid).update(transaction);
    }

    getOngoingTransactionById() {
        let transactionRef: firebase.database.Reference = firebase.database().ref(`/transaction_history/`);
        let result = [];
        transactionRef.on('value', data => {
            data.forEach(element => {
                if (element.child(`/creditor`).val() == this.currentUser.uid || element.child(`/debtor`).val() == this.currentUser.uid) {
                    if (element.child(`/status`).val() == 'ongoing') {
                        result.push(element.val());
                    }
                    return false;
                }
            });
        });
        return result;
    }

    getTransactionById(uid: string) {
        let transactionRef: firebase.database.Reference = firebase.database().ref(`/transaction_history/`);
        let result = [];
        transactionRef.on('value', data => {
            data.forEach(element => {
                if (element.child(`/uid`).val() == uid) {
                    result.push(element.val());
                    return false;
                }
            });
        });
        return result;
    }

    getDoneLendTransaction() {
        let transactionRef: firebase.database.Reference = firebase.database().ref(`/transaction_history/`);
        let result = [];
        transactionRef.on('value', data => {
            data.forEach(element => {
                if (element.child(`/creditor`).val() == this.currentUser.uid) {
                    if (element.child(`/status`).val() == 'done') {
                        result.push(element.val());
                    }
                    return false;
                }
            });
        });
        return result;
    }

    getDoneBorrowTransaction() {
        let transactionRef: firebase.database.Reference = firebase.database().ref(`/transaction_history/`);
        let result = [];
        transactionRef.on('value', data => {
            data.forEach(element => {
                if (element.child(`/debtor`).val() == this.currentUser.uid) {
                    if (element.child(`/status`).val() == 'done') {
                        result.push(element.val());
                    }
                    return false;
                }
            });
        });
        return result;
    }

    generateUniqueId(len: number) {
        var text = "";
        var charset = "abcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < len; i++)
            text += charset.charAt(Math.floor(Math.random() * charset.length));

        return text;
    }

    cancelTransaction(){

    }
}