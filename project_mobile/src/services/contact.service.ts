import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { Contact } from '../models/contact.interface';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class ContactService {

  private currentUser = firebase.auth().currentUser;
  // private contactListRef = this.db.list<Contact>('contact/' + this.currentUser.uid);
  private contactListRef = firebase.database().ref('contact/' + this.currentUser.uid);

  constructor(private db: AngularFireDatabase) { }

  getContactList() {
    // return this.contactListRef;
    // return this.db.list('contact/' + this.currentUser.uid).valueChanges();
    return firebase.database().ref('contact/' + this.currentUser.uid);
  }

  addContact(c: Contact) {
    let pushedKey = c.key;
    return firebase.database().ref('contact/' + this.currentUser.uid + '/' + pushedKey).update(c);
    // return this.contactListRef.push(c);
  }

  removeContact(c: Contact) {
    return this.contactListRef.remove();
  }
}