import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { ContactDetailPage } from '../contact-detail/contact-detail';
import { DatabaseService } from '../../services/database.service';

import { ContactService } from '../../services/contact.service';
import { Observable } from 'rxjs/Observable';
import { Contact } from '../../models/contact.interface';
import { AddContactPage } from '../add-contact/add-contact';
import { AngularFireList } from 'angularfire2/database';
import * as firebase from 'firebase';

// @IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {

  contactList: Contact[] = [];

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    private contactService: ContactService,
    public alertCtrl: AlertController,
    public dbService: DatabaseService) {
      
  }

  ionViewDidEnter() {
    this.getAllContact();
  }

  getAllContact() {
    let ref = this.contactService.getContactList();
    ref.once('value', snapshot => {
      //use this to clear the array to prevent duplicate
      this.contactList.splice(0, this.contactList.length);
      snapshot.forEach(item => {
        let x = item.val();
        //push each item on the reference into the array
        this.contactList.push(x);
      });
    });
  }

  toDetailPage(c: Contact){
    this.navCtrl.push(ContactDetailPage, {params: c});
  }

  moveToAddNewContact() {
    // console.log("move to add new contact page button is works!");
    this.navCtrl.push(AddContactPage, {params: this.contactList});
  }

}


// deleteContact(c: Contact) {
  //   let ref = firebase.database().ref('contact/' + firebase.auth().currentUser.uid + '/' + c.key);

  //   let alert = this.alertCtrl.create({
  //     message: 'Are you sure ?',
  //     buttons: [
  //       {
  //         text: 'Yes',
  //         handler: data => {
  //           console.log("Yes is pressed!");
  //           ref.remove().then(() => {
  //             this.getAllContact();
  //           })
  //           .catch(() => {
  //             this.getAllContact();
  //           })
  //         }
  //       },
  //       {
  //         text: 'No',
  //         role: 'cancel',
  //         handler: data => {
  //           console.log("Cancel is pressed!");
  //         }
  //       }
  //     ]
  //   }).present();
  // }
