import { Component,NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as firebase from 'firebase';
import { DatabaseService } from '../../services/database.service';
import { User } from '../../data/user';
import { TabsPage } from '../tabs/tabs';

// @IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {

  private imageSrcGallery: string;

  private imageSrcCamera:string;
  private capturedDataUrl:any;
  private pictureUploaded:boolean;
  
  profileForm: FormGroup;
  minDate: any;
  today: string;
  errorDate: any = false;
  username: any;
  dob: any;
  gender: any;
  name: any;
  phone: any;

  currentUserId: any = firebase.auth().currentUser.uid;
  currentUserEmail: any = firebase.auth().currentUser.email;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public camera: Camera,
    public formBuilder: FormBuilder,

    public dbService: DatabaseService,
    public zone: NgZone) {
    this.minDate = new Date().getFullYear() - 100;
    this.today = new Date().toISOString().split('T')[0];
    this.pictureUploaded=false;
    this.profileForm = this.formBuilder.group({
      username: ['', Validators.required],
      name: ['', Validators.required],
      dob: [''],
      gender: [''],
      phone: ['', Validators.required]
    })
  }

  ionViewDidEnter(){
    for(let i of this.dbService.getUserById(this.currentUserId)){
      this.username = i.username;
      this.dob = i.dob;
      this.name = i.name;
      this.phone = i.phone;
      this.gender = i.gender;
    }
  }

  checkDate() {
    let check: string = this.profileForm.value.dob;
    if (check < this.today && check != null) {
      this.errorDate = true;
    }
    else {
      this.errorDate = false;
    }
  }

  checkMale(){
    if(this.gender == 'm') return true;
  }

  checkFemale(){
    if(this.gender == 'f') return true;
  }

  onSubmitForm() {
    let userData: any;
    userData = this.dbService.getUserById(this.currentUserId);

    let data: User = new User();
    data.uid = this.currentUserId;
    data.username = this.profileForm.value.username;
    data.name = this.profileForm.value.name;
    data.email = this.currentUserEmail;
    data.dob = this.profileForm.value.dob;
    data.gender = this.profileForm.value.gender;
    data.phone = this.profileForm.value.phone;
    
    let debt_total = this.dbService.getDebtById(this.currentUserId).toString();
    let credit_total = this.dbService.getCreditById(this.currentUserId).toString();
    data.debt_total = parseInt(debt_total);
    data.credit_total = parseInt(credit_total);

    this.dbService.addUser(data);
    this.navCtrl.setRoot(TabsPage);
  }

  openGallery() {
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.imageSrcGallery = 'data:image/jpg;base64,' + imageData;
    }, (err) => {
      // Handle error
    });
  }
  openCamera() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.imageSrcCamera = 'data:image/jpg;base64,' + imageData;
    }, (err) => {
      // Handle error
    });
  }

  uploadImage() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    let uploadedImage: any;
    this.camera.getPicture(options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
     uploadedImage = 'data:image/jpg;base64,' + imageData;
    this.dbService.uploadImage(uploadedImage,'profile_'+this.currentUserId);
    this.pictureUploaded=true;
    this.fetchPicture();
    }, (err) => {
      // Handle error
    });
  }

  fetchPicture() {
// uid is user id you can ignore uid if you dont want it.

    firebase.storage().ref().child('image').child('profile_'+this.currentUserId).getDownloadURL().then((url) => {
         this.zone.run(() => {
        this.capturedDataUrl = url;
        console.log(this.capturedDataUrl);
       })
    })
  }
}
