import { Component,NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, App, LoadingController, ToastController, AlertController, PopoverController } from 'ionic-angular';
import { DatabaseService } from '../../services/database.service';
import { LoginPage } from '../login/login';
import * as firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { EditProfilePage } from '../edit-profile/edit-profile';
import { PopoverControllerPage } from '../popover-controller/popover-controller';

// @IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  verified: boolean = false;
  currentUser: any = firebase.auth().currentUser;
  currentUserId: any = firebase.auth().currentUser.uid;
  debt: any;
  credit: any;
  name: any;
  phone: any;
  private capturedDataUrl:any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public dbService: DatabaseService,
    public appCtrl: App,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private aFAuth: AngularFireAuth,
    public popoverCtrl: PopoverController,
    public zone:NgZone
  ) {
  }

  presentPopover(event) {
    let popover = this.popoverCtrl.create(PopoverControllerPage);
    popover.present({
      ev: event
    });
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad ProfilePage');
    const authObserver = this.aFAuth.authState.subscribe(user => {
      if (user && user.emailVerified) {
        this.verified = true;
        authObserver.unsubscribe();
      }
    });

    //get profile picture
    firebase.storage().ref().child('image').child('profile_'+this.currentUserId).getDownloadURL().then((url) => {
      this.zone.run(() => {
     this.capturedDataUrl = url;
     console.log(this.capturedDataUrl);
    })
 })
  }

  ionViewDidEnter(){
    for(let i of this.dbService.getUserById(this.currentUserId)){
      this.debt = i.debt_total;
      this.credit = i.credit_total;
      this.name = i.name;
      this.phone = i.phone;
    }
  }

  logout() {
    this.presentLoadingToLogout();
    this.dbService.logout();

  }

  presentLoadingToLogout() {
    let loading = this.loadingCtrl.create({
      content: 'Signing out...',
      duration: 5000
    });

    loading.onDidDismiss(() => {
      let toast = this.toastCtrl.create({
        message: 'Signed out',
        duration: 3000,
        position: 'bottom'
      });

      toast.present();

      this.appCtrl.getRootNav().setRoot(LoginPage);
    });

    loading.present();
  }

  sendVerificationEmail() {
    //Send email verification
    let verify = this.currentUser.sendVerificationEmail();

    let alert = this.alertCtrl.create({
      message: 'Verification email sent to' + this.currentUser.email,
      buttons: ['Dismiss']
    }).present();
  }

  moveToEditProfile() {
    this.navCtrl.push(EditProfilePage);
  }

}
