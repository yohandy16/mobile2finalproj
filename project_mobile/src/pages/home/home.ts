import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { OngoingDetailPage } from '../ongoing-detail/ongoing-detail';
import { TransactionService } from '../../services/transaction.service';
import { Transaction } from '../../data/transaction';
import firebase from 'firebase';
import { User } from '../../data/user';
import { DatabaseService } from '../../services/database.service';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  transactionList: Transaction[] = [];
  user: any = [];
  currentUser = firebase.auth().currentUser.uid;
  debt: any;
  credit: any;
  name: any;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private trscService: TransactionService,
    private dbService: DatabaseService) {
  }

  toOngoingPage(uid: string){
    this.navCtrl.push(OngoingDetailPage, {uid: uid});
  }

  ionViewDidEnter(){
    this.transactionList = this.trscService.getOngoingTransactionById();
    for(let i of this.dbService.getUserById(this.currentUser)){
      this.debt = i.debt_total;
      this.credit = i.credit_total;
      this.name = i.name;
    }
  }
}
