import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormGroup, Validators, AbstractControl, FormBuilder, FormArray } from '@angular/forms';
import { TabsPage } from '../tabs/tabs';
import { TransactionService } from '../../services/transaction.service';
import { Transaction } from '../../data/transaction';
import * as firebase from 'firebase';
import { ContactService } from '../../services/contact.service';
import { Contact } from '../../models/contact.interface';

// @IonicPage()
@Component({
  selector: 'page-add',
  templateUrl: 'add.html',
})
export class AddPage {

  addForm: FormGroup;
  name: AbstractControl;
  type: AbstractControl;
  amount: AbstractControl;
  date: AbstractControl;
  description: AbstractControl;
  // due: AbstractControl;
  minDate: any;
  maxDate: any;
  today: string = new Date().toISOString().split('T')[0];
  errorDate: any = false;
  errorDue: any = false;
  errorAmount: any = false;
  currentUser: any = firebase.auth().currentUser;
  contactList: Contact[] = [];
  // person: any = [];

  constructor(private navCtrl: NavController, 
    private navParams: NavParams,
    private formBuilder: FormBuilder,
    private transactionService: TransactionService,
    private contactService: ContactService) {
      this.minDate = new Date().getFullYear();
      this.maxDate = this.minDate + 2;
  }

  ngOnInit(){
    this.initializeForm();
  }

  ionViewDidEnter(){
    let ref = this.contactService.getContactList();
    ref.once('value', snapshot => {
      this.contactList.splice(0, this.contactList.length);
      snapshot.forEach(item => {
        let x = item.val();
        this.contactList.push(x);
      });
    });
  }

  private initializeForm(){
    this.addForm = this.formBuilder.group({
      type: [null, Validators.required],
      name: [null, Validators.required],
      amount: [null, Validators.required],
      date: ['', Validators.required],
      description: ['', Validators.required]
    });

    this.type = this.addForm.controls['type'];
    this.name = this.addForm.controls['name'];
    this.amount = this.addForm.controls['amount'];
    this.date = this.addForm.controls['date'];
    this.description = this.addForm.controls['description'];
  }

  onAddTransaction(){
    let newTransaction: Transaction = new Transaction();
    newTransaction.amount = this.addForm.get('amount').value;
    newTransaction.type = this.addForm.get('type').value;
    newTransaction.description = this.addForm.get('description').value;
    newTransaction.transactionDate = this.addForm.get('date').value;
    newTransaction.payDate = '';
    newTransaction.status = 'ongoing';
    if(this.addForm.get('type').value == 'lend'){
      newTransaction.creditor = this.currentUser.uid;
      newTransaction.debtor = this.addForm.get('name').value;
    }
    if(this.addForm.get('type').value == 'borrow'){
      newTransaction.debtor = this.currentUser.uid;
      newTransaction.creditor = this.addForm.get('name').value;
    }
    this.transactionService.addTransaction(newTransaction);
    this.addForm.reset();
    this.navCtrl.push(TabsPage);
  }

  checkDate(){
    let check: string = this.addForm.get('date').value;
    if(check < this.today && check != null){
      this.errorDate = true;
    }
    else this.errorDate = false;
  }

  // checkDue(){
  //   let due: string = this.addForm.get('due').value;
  //   let date: string = this.addForm.get('date').value;
  //   if(due < date && due != null && date != null){
  //     this.errorDue = true;
  //   }
  //   else this.errorDue = false;
  // }

  checkAmount(){
    let amount: any = parseInt(this.addForm.get('amount').value);
    if(amount < 1000){
      this.errorAmount = true;
    }
    else this.errorAmount = false;
  }

  // addPerson(){
  //   // if(this.person.length < 2){
  //     this.person.push({'value':''});
  //   // }
  // }
}