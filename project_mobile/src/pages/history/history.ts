import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HistoryDetailPage } from '../history-detail/history-detail';
import { Transaction } from '../../data/transaction';
import { TransactionService } from '../../services/transaction.service';

// @IonicPage()
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {

  transactionList: Transaction[] = [];
  type: any = 'lend';

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private trscService: TransactionService) {
  }

  toDetailPage(uid: string){
    this.navCtrl.push(HistoryDetailPage, {uid: uid});
  }

  getHistory(type: any){
    if(type == 'lend'){
      this.transactionList = this.trscService.getDoneLendTransaction();
    }
    if(type == 'borrow'){
      this.transactionList = this.trscService.getDoneBorrowTransaction();
    }
    return this.transactionList;
  }
}
