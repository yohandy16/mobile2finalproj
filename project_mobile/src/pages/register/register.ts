import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../../data/user';
import { DatabaseService } from '../../services/database.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as firebase from 'firebase';
import { ProfilePage } from '../profile/profile';
import { TabsPage } from '../tabs/tabs';
import { EditProfilePage } from '../edit-profile/edit-profile';

// @IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  users: User[] = [];
  usersForm: FormGroup;
  debt_total: number;
  credit_total: number;
  // user = {'email': '', 'password': ''};
  // email: string;
  // password: string;

  constructor(public toastCtrl: ToastController,
    private aFAuth: AngularFireAuth,
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public dbService: DatabaseService,
    public formBuilder: FormBuilder) {

    this.usersForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  async onSubmitForm() {
    try {
      const result = await this.aFAuth.auth.createUserWithEmailAndPassword(this.usersForm.value.email, this.usersForm.value.password);
      console.log(this.usersForm.value);

      if (result) {

        let datauser = this.dbService.getUsers().on('value', usersList => {
          usersList.forEach(element => {
            this.users.push(element.val())
          });
        });

        let data: User = new User();
        data.uid = result.user.uid;
        // data.uid = this.dbService.generateUniqueId(5);
        data.email = this.usersForm.value.email;
        data.debt_total = 0;
        data.credit_total = 0;
        console.log(data);

        this.dbService.addUser(data);

        let alert = this.alertCtrl.create({
          title: 'Registration success!',
          subTitle: '',
          buttons: [
            {
              text: 'Ok',
              handler: data => {
                //Send email verification
                //let verify = firebase.auth().currentUser.sendEmailVerification().
                const result = this.aFAuth.auth.signInWithEmailAndPassword(this.usersForm.value.email, this.usersForm.value.password);

                this.navCtrl.setRoot(TabsPage);
                this.navCtrl.setRoot(EditProfilePage);
              }
            }
          ]
        }).present();
      }
    }
    catch (e) {
      // console.error(e);
      let alert = this.alertCtrl.create({
        title: 'Oops',
        buttons: ['Dismiss']
      });

      if (e.code == 'auth/email-already-in-use') {
        alert.setMessage('The email is already registered!');
        alert.present();
      } else if (e.code == 'auth/weak-password') {
        alert.setMessage('Please consider to use stronger password');
        alert.present();
      } else if (e.code == 'auth/invalid-email') {
        alert.setMessage('Email is invalid! Please use valid email address');
        alert.present();
      }
    }

  }



  // async register(email: string, password: string) {
  //   try {
  //     const result = await this.aFAuth.auth.createUserWithEmailAndPassword(email, password);
  //     console.log(email,);

  //     if (result) {

  //       // var i: number = 0;
  //       let datauser = this.dbService.getUsers().on('value', userList => {
  //         userList.forEach(element => {
  //           this.users.push(element.val())
  //         });
  //       });

  //       let data: User = new User();
  //       data.email = email;
  //       data.password = password;

  //       this.dbService.addUser(data);

  //       let toast = this.toastCtrl.create({
  //         message: 'Add user success',
  //         duration: 3000,
  //         position: 'bottom'
  //       });

  //       toast.onDidDismiss(() => {
  //         this.navCtrl.pop();
  //       })

  //       toast.present();

  //     }
  //   }
  //   catch (e) {
  //     console.error(e);
  //   }
  // }

}
