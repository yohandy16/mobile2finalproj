import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { DatabaseService } from '../../services/database.service';
import { Contact } from '../../models/contact.interface';
import { ContactService } from '../../services/contact.service';
import * as firebase from 'firebase';

// @IonicPage()
@Component({
  selector: 'page-contact-detail',
  templateUrl: 'contact-detail.html',
})
export class ContactDetailPage {

  contactDetail: any;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public contactService: ContactService,
    public alertCtrl: AlertController,
    private dbService: DatabaseService) {
      this.contactDetail = navParams.get('params');
      console.log(this.contactDetail);
  }

  ionViewDidLoad() {
    // this.contactDetail = this.dbService.getUserById(this.navParams.get('uid'));
    // console.log(this.contactDetail);
  }

  deleteContact() {
    let ref = firebase.database().ref('contact/' + firebase.auth().currentUser.uid + '/' + this.contactDetail.key);

    let alert = this.alertCtrl.create({
      message: 'Are you sure ?',
      buttons: [
        {
          text: 'Yes',
          handler: data => {
            console.log("Yes is pressed!");
            ref.remove().then(() => {
              this.navCtrl.pop();
            })
            .catch(() => {
              this.navCtrl.pop();
            })
          }
        },
        {
          text: 'No',
          role: 'cancel',
          handler: data => {
            console.log("Cancel is pressed!");
          }
        }
      ]
    }).present();
  }

}
