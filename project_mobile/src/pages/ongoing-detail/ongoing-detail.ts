import { Component,NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Transaction } from '../../data/transaction';
import { TransactionService } from '../../services/transaction.service';
import { TabsPage } from '../tabs/tabs';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { DatabaseService } from '../../services/database.service';
import * as firebase from 'firebase';

// @IonicPage()
@Component({
  selector: 'page-ongoing-detail',
  templateUrl: 'ongoing-detail.html',
})
export class OngoingDetailPage {
  private capturedDataUrl:any;
  transaction: Transaction;
  type: any;
  today: string = new Date().toISOString().split('T')[0];
  currentUserId: any = firebase.auth().currentUser.uid;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private trscService: TransactionService,
    private camera:Camera,
    private dbService:DatabaseService,
    private zone:NgZone) {
  }

  ionViewDidLoad() {
    for(let i of this.trscService.getTransactionById(this.navParams.get('uid'))){
      this.transaction = i;
    }
    this.type = this.transaction.type;
  }

  upload(){

  }

  payDebt(){
    let newTransaction: Transaction = new Transaction();
    newTransaction.uid = this.transaction.uid;
    newTransaction.amount = this.transaction.amount;
    newTransaction.type = this.transaction.type;
    newTransaction.description = this.transaction.description;
    newTransaction.transactionDate = this.transaction.transactionDate;
    newTransaction.payDate = this.today;
    newTransaction.status = 'done';
    newTransaction.creditor = this.transaction.creditor;
    newTransaction.debtor = this.transaction.debtor;
    
    this.trscService.editTransaction(newTransaction);
    this.navCtrl.push(TabsPage);
  }

  uploadImage() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    let uploadedImage: any;
    this.camera.getPicture(options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
     uploadedImage = 'data:image/jpg;base64,' + imageData;
    this.dbService.uploadImage(uploadedImage,'transaction_'+this.transaction.uid);
    this.fetchPicture();
    }, (err) => {
      // Handle error
    });
  }

  fetchPicture() {
// uid is user id you can ignore uid if you dont want it.

    firebase.storage().ref().child('image').child('transaction_'+this.transaction.uid).getDownloadURL().then((url) => {
         this.zone.run(() => {
        this.capturedDataUrl = url;
        console.log(this.capturedDataUrl);
       })
    })
  }

}
