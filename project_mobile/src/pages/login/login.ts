import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { User } from '../../models/user.interface';
import { AngularFireAuth } from 'angularfire2/auth';
import { TabsPage } from '../tabs/tabs';
import { RegisterPage } from '../register/register';

// @IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  splash = true;
  

  user = {} as User;

  constructor(public toastCtrl: ToastController, 
    public alertCtrl: AlertController,
    private aFAuth: AngularFireAuth, 
    public navCtrl: NavController, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    setTimeout(() => this.splash = false, 4000);
  }

  async login(user: User) {
    try{
      const result = await this.aFAuth.auth.signInWithEmailAndPassword(user.email, user.password);
      console.log(result);
      if(result) {
        let toast = this.toastCtrl.create({
          message: 'Hello!' + result.user.email + '!',
          duration: 100,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          this.navCtrl.setRoot(TabsPage);
        })

        toast.present();
      }
    }
    catch(e) {
      // console.error(e);
      let alert = this.alertCtrl.create({
        title: 'Invalid credentials',
        subTitle: 'Invalid credentials',
        buttons: ['Dismiss']
      }).present();
    }
  }

  register() {
    this.navCtrl.push(RegisterPage);
  }

}