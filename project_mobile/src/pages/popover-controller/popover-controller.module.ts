import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopoverControllerPage } from './popover-controller';

@NgModule({
  declarations: [
    PopoverControllerPage,
  ],
  imports: [
    IonicPageModule.forChild(PopoverControllerPage),
  ],
})
export class PopoverControllerPageModule {}
