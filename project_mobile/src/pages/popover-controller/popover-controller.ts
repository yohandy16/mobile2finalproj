import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, ToastController, App } from 'ionic-angular';
import { DatabaseService } from '../../services/database.service';
import { LoginPage } from '../login/login';

@IonicPage()
@Component({
  selector: 'page-popover-controller',
  template: `
  <ion-list>
    <ion-list-header>More</ion-list-header>
    <button ion-item (click)="logout()">Log Out</button>
  </ion-list>`
})
export class PopoverControllerPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public dbService: DatabaseService, 
    public viewCtrl: ViewController, 
    public loadingCtrl: LoadingController, 
    public toastCtrl: ToastController,
    public appCtrl: App) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoverController');
  }

  logout(){
    this.dbService.logout();
    this.presentLoadingToLogout();
    // this.navCtrl.setRoot(LoginPage);
    this.viewCtrl.dismiss();
  }

  presentLoadingToLogout() {
    let loading = this.loadingCtrl.create({
      content: 'Signing out...',
      duration: 5000
    });

    loading.onDidDismiss(() => {
      let toast = this.toastCtrl.create({
        message: 'Signed out',
        duration: 3000,
        position: 'bottom'
      });

      toast.present();

      this.appCtrl.getRootNav().setRoot(LoginPage);
    });

    loading.present();
  }
}
