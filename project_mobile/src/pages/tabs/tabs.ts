import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
import { ContactPage } from '../contact/contact';
import { HistoryPage } from '../history/history';
import { HomePage } from '../home/home';
import { AddPage } from '../add/add';

// @IonicPage()
@Component({
  selector: 'page-tabs',
  template: `
  <ion-tabs class="tabs-md">
    <ion-tab [root]="homePage" tabIcon="custom-home"></ion-tab>
    <ion-tab [root]="contactPage" tabIcon="custom-contact"></ion-tab>
    <ion-tab [root]="addPage" tabIcon="custom-money"></ion-tab>
    <ion-tab [root]="historyPage" tabIcon="custom-history"></ion-tab>
    <ion-tab [root]="profilePage" tabIcon="custom-user"></ion-tab>
  </ion-tabs>`
})

// 

export class TabsPage {

  profilePage = ProfilePage;
  homePage = HomePage;
  contactPage = ContactPage;
  historyPage = HistoryPage;
  addPage = AddPage;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  toAddPage(){
    this.navCtrl.push(AddPage);
  }


}
