import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ContactService } from '../../services/contact.service';
import { Contact } from '../../models/contact.interface';
import { User } from '../../data/user';
import * as firebase from 'firebase';

// @IonicPage()
@Component({
  selector: 'page-add-contact',
  templateUrl: 'add-contact.html',
})
export class AddContactPage {

  C: Contact = {
    email: '',
    uid: 'qweqweqweqwe'
  }

  form_email: string;
  
  currentUser: any = firebase.auth().currentUser;
  currentContactList: any;
  U: User[] = [];

  public ada: boolean = false;
  udahAdaDiKontak: boolean;

  constructor(public navCtrl: NavController, 
    private contactService: ContactService,
    public navParams: NavParams,
    public alertCtrl: AlertController
    ) {
      this.currentContactList = navParams.get('params');
      console.log(this.currentContactList);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddContactPage');
    // console.log(this.U);
  }

  ionViewDidEnter() {
    console.log('Import user data...');
    let ref = firebase.database().ref('users');
    ref.on('value', userList => {
      userList.forEach(element => {
        this.U.push(element.val());
      });
    });
  }

  getUIDbyEmail(findEmail) {
    for(let data of this.U){
      if(data.email == findEmail) {
        this.ada = true;
        return data.uid;
      }
    }

    return -9;
  }

  checkIfEmailIsAlreadyInContactList(findEmail) {
    for(let data of this.currentContactList){
      if(data.email == findEmail) {
        return true;
      }
    }

    return false;
  }

  submitForm(formEmail: string) {
    if(formEmail == this.currentUser.email){
      let alert = this.alertCtrl.create({
        message: 'You cannot add yourself',
        buttons: ['Dismiss']
      }).present();
    } else {
      this.udahAdaDiKontak = this.checkIfEmailIsAlreadyInContactList(formEmail);
      if(this.udahAdaDiKontak){
        let alert = this.alertCtrl.create({
          message: 'You already have his/her in your contact',
          buttons: ['Dismiss']
        }).present();
      } else {
        let uid = this.getUIDbyEmail(formEmail);
        console.log(this.ada);
        if(this.ada){
          this.addContact(formEmail, uid);
        } else {
          let alert = this.alertCtrl.create({
            message: 'This email is not registered in our database',
            buttons: ['Dismiss']
          }).present();
        }
      }
    }
  }

  addContact(email: string, uid: any) {
    let pushedKey = this.contactService.getContactList().push().key;

    let Kontak = {
      email: email,
      uid: uid,
      key: pushedKey
    } as Contact;

    this.contactService.addContact(Kontak).then(ref => {
      let alert = this.alertCtrl.create({
        message: 'Successfully add contact!',
        buttons: [
          {
            text: 'Yeay!',
            handler: data => {

            }
          }
        ]
      }).present();
      this.navCtrl.pop();
    });
  }

}
