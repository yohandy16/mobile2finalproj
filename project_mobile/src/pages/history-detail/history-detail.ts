import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TransactionService } from '../../services/transaction.service';
import { Transaction } from '../../data/transaction';
import { DatabaseService } from '../../services/database.service';

// @IonicPage()
@Component({
  selector: 'page-history-detail',
  templateUrl: 'history-detail.html',
})
export class HistoryDetailPage {

  type: any;
  amount: any
  description: any;
  userId: any;
  name: any;
  email: any;
  today: string = new Date().toISOString().split('T')[0];

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private trscService: TransactionService,
    private dbService: DatabaseService) {
  }

  ionViewDidLoad() {
    for(let i of this.trscService.getTransactionById(this.navParams.get('uid'))){
      this.type = i.type;
      this.amount = i.amount;
      this.description = i.description;
      if(this.type == 'lend'){
        this.userId = i.debtor;
      }
      if(this.type == 'borrow'){
        this.userId = i.creditor;
      }
    }
    for(let i of this.dbService.getUserById(this.userId)){
      this.name = i.name;
      this.email = i.email;
    }
  }

}
