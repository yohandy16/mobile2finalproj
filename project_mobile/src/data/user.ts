export class User {
    uid: string;
    username: string;
    name: string;
    email: string;
    password: string;
    dob: string;
    gender: string;
    phone: string;
    auth_key: string;
    debt_total: number;
    credit_total: number;
    contact_id: {
       cid: string;
    }
}