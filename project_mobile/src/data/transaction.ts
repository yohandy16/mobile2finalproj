export class Transaction {
    uid: string;
    amount: number;
    type: string;
    description: string;
    transactionDate: string;
    payDate: string;
    debtor: string;
    creditor: string;
    status: string;
}