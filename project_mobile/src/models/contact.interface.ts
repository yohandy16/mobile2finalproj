export interface Contact {
  key?: string,
  email: string,
  uid: string
}